﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace Event
{
    class Robot
    {
        public delegate void MethodContainer();
        public event MethodContainer onWrite;
        private Random rand;
        private static readonly string[] direction = { "вверх", "вниз", "влево", "вправо" };

        public Robot()
        {
            rand = new Random();
        }

        public void Steps(object nSteps)
        {
            for (int i = 0; i < (int)nSteps; ++i)
            {
                int buf = rand.Next(4);
                Console.WriteLine("Робот поехал " + direction[buf]);
                if (buf == 1) onWrite();
            }
        }
    }

    class Handler
    {
        public void Write()
        {
            using (StreamWriter sw = new StreamWriter("log.txt", true, System.Text.Encoding.Default))
            {
                sw.WriteLine("Робот поехал назад");
                sw.Close();
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Robot robot = new Robot();
            Handler handle = new Handler();
            robot.onWrite += handle.Write;

            Thread thread = new Thread(new ParameterizedThreadStart(robot.Steps));
            thread.Start(10);

            Console.ReadKey();
        }
    }
}
